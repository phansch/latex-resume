# Development

**Installing pdflatex + texlive**

    sudo apt-get install texlive texlive-lang-german

**Re-generating the PDF**

    pdflatex resume-philipp-hansch.tex
